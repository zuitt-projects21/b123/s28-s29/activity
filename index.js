//loading the xpress.js module into our application and into the express variable.
//express module will allow us to use exressjs methods to create our API.
const express = require("express");

//creates an application with expressjs
//this creates an expressjs application and stores it as an app. 
//app is our server.
const app = express();

//port is a variable to contain the port we want to designate
const port = 4000;

app.use(express.json());

let users = [

	{
		email:"mighty12@gmail.com",
		username: "mightyMouse",
		password: "notrelatedtomickey",
		isAdmin: false
	},
	{
		email:"minnieMouse@gmail.com",
		username: "minniexmickey",
		password: "minniesincethestart",
		isAdmin: false
	},
	{
		email:"mickeyTheMouse@gmail.com",
		username: "mickeyKing",
		password: "thefacethatrunstheplace",
		isAdmin: true
	}

];

let items = [

	{
		name: "Doritos",
		description: "Corn Chips",
		price: 180,
		isActive: true
	},
	{
		name: "Lays",
		description: "Potato Chips",
		price: 150,
		isActive: true
	},
	{
		name: "Tomi",
		description: "Sweet Corn Chips",
		price: 18,
		isActive: true
	}

];

let loggedUser;

//Express has methods to use as routes corresponding to each HTTP method.
//get(<endpoint>,<functiontoHandle request and responses>)
app.get('/', (req,res) => {

	//once the route is accessed, we can send a response with the use of res.send()
	//res.send() actually combines writeHead() and end() already. no need to specify content-type.
	//It is used to send a response to the client and is the end of the response.
	res.send("Hello World!");

})

app.get('/hello', (req,res)=>{

	res.send("Hello from Batch 123!");
})

app.post('/', (req,res)=>{

	console.log(req.body);
	res.send(`Hello, I'm ${req.body.name}. I am ${req.body.age}. I could be described as ${req.body.description} `);
	/*res.send("Hello, I like sleeping!");*/
})

app.post('/users',(req,res)=>{
	console.log(req.body);

	let newUser = {

		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	}

	users.push(newUser);
	console.log(users);

	res.send(`Registration Successful.`);
//note: post is best if youre passing data without updating
})

//login
app.post('/users/login',(req,res)=>{
	console.log(req.body);
	
	let foundUser = users.find((user)=>{
		return user.username === req.body.username && user.password === req.body.password;
	})


	console.log(foundUser);
	
	if (foundUser !== undefined){
	
		loggedUser = foundUser;
		console.log(loggedUser);
		res.send('Thank you for logging in.')
		
		//for an array of objects, we can use findIndex. will iterate over all the items and return the index of the current item that matches the return condition.
		let foundUserIndex = users.findIndex((user)=>{

			return user.username === foundUser.username

		});
		foundUser.index = foundUserIndex;
		console.log(foundUser)

	} else{
		loggedUser = foundUser;
		res.send(`Login failed. Wrong Credentials.`)
	}

}) 

//Add item
app.post('/items',(req,res)=>{
	console.log(loggedUser);
	console.log(req.body);
	
	//activity part:
	let newItem = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.name,
		isActive: req.body.name
	};

	console.log(loggedUser.isAdmin);

	if(loggedUser.isAdmin === true){
		items.push(newItem);
		console.log(items);
		res.send('You have added a new item.')
	} else{
		res.send('Unauthorized: Action Forbidden')
	} 

})

app.get('/items', (req,res)=>{
	
	console.log(loggedUser.isAdmin);

	if(loggedUser.isAdmin === true){
		res.send(items);
	} else{
		res.send('Unauthorized: Action Forbidden')
	} 

})

//getSingleUser
	//GET requests should not have a request body. It may have headers for additional information or we can add a small amount of data somewhere else: the url.
	//Route params are values we can pass via the URL.
	//This is done especially to allow us to send small amount of adata into our server.
	//Route parameters can be defined in the endpoint of a route with :parameterName
	
	app.get('/users/:index',(req,res)=>{

		console.log(req.params);
		//req.params is an object that contains the route params
		//its properties are then determined by your route parameters
		//route params (ex. :index or :number would be the property name.)

		console.log(req.params.index);
		//at this point - it's still unusable since value is a string. what we can do: parseInt
		let index = parseInt(req.params.index);
		/*console.log(typeof index);*/

		let user = users[index];
		res.send(user);

	})	

//updateUser
	//password update. Get user first. To do this, we should not pass the index of the user in the body but instead in our route params.

	app.put('/users/:index', (req,res)=>{

		console.log(req.params);
		console.log(req.params.index);
		
		let userIndex = parseInt(req.params.index);

		if(loggedUser !== undefined && loggedUser.index === userIndex){
			users[userIndex].password = req.body.password ;
			console.log(users[userIndex]);
			res.send('User password has been updated.')
		} else{
			res.send('Unauthorized. Login the correct user first.')
		}

	})

//getSingleItem
	app.get('/items/getSingle/:index',(req,res)=>{

		res.send(items[req.params.index]);

	})

//archiveItem
	app.put('/items/archive/:index',(req,res)=>{

		let itemIndex = parseInt(req.params.index);

		if(loggedUser !== undefined && loggedUser.isAdmin === true){
			items[itemIndex].isActive = false;
			res.send('Item Archived.');
		} else{
			res.send('Unauthorized: Action Forbidden.');
		}

		console.log(items[itemIndex]);
		
	})

//activateItem
	
	app.put('/items/activate/:index',(req,res)=>{

		let itemIndex = parseInt(req.params.index);

		if(loggedUser !== undefined && loggedUser.isAdmin === true){
			items[itemIndex].isActive = true;
			res.send('Item activated.');
		} else{
			res.send('Unauthorized: Action Forbidden.');
		}

		console.log(items[itemIndex]);
		
	})


//always at the end
app.listen(port, () => console.log(`Server is running at port ${port}`));